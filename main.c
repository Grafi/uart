#include <avr/interrupt.h>
#include <string.h>

#include "uart.h"

int main(void)
{

    uart_init(9600);
    

    /* Enable interrupts */
    sei();


    while (1)
    {
		uart_print("Button released\n\r");
    }
	

    return 0;
}
